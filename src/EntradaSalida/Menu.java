package EntradaSalida;

import Excepciones.EntradaDatosException;
import java.io.IOException;

public class Menu extends OpcionMenu{
    private final OpcionMenu[] opcionesDelMenu;
    private String opcionElegida;
    private int opcionesIntroducidas; //variable para saber el número de objetos opcionMenu que hay en el vector opcionesDelMenu   
    
    public Menu(String titulo, int numeroOpciones){
        super(titulo, "MENU");
        opcionesDelMenu = new OpcionMenu[numeroOpciones];
        opcionesIntroducidas = 0; 
    }
        
    @Override
    public void iniciar() {
        mostrarOpciones();
        pedirEntradaDatos();
    }
    
    public void mostrarOpciones(){
        pintarCabezera();
        
        for (int i = 0; i < opcionesDelMenu.length; i++) {
            System.out.println(i+1 + ". " + opcionesDelMenu[i].getTitulo());
        }
    }

    @Override
    public void pedirEntradaDatos() {
        int numeroOpcionIntroducida = 0;
        boolean seguirPidiendoEntrada = true;
        
        do{
            try{
                System.out.print("\n· Introduce la opción que quieres ejecutar [0 para salir de la aplicación]-> ");
                numeroOpcionIntroducida = Integer.parseInt(IN.readLine());
                validarOpcion(numeroOpcionIntroducida);
                seguirPidiendoEntrada=false;
            }catch(NumberFormatException e){
                System.err.println("El tipo de dato introducido no es correcto.");
            }catch(EntradaDatosException | IOException e){
                System.err.println(e);
            }
        }while(seguirPidiendoEntrada);

        if (numeroOpcionIntroducida == 0){
            opcionElegida = "SALIR";
        }else{
            opcionElegida = opcionesDelMenu[numeroOpcionIntroducida-1].getID();
        }
    }

    private void validarOpcion(int opcionIntroducida) throws EntradaDatosException {
        
        if(opcionIntroducida > opcionesDelMenu.length || opcionIntroducida < 0){
            throw new EntradaDatosException(String.valueOf(opcionIntroducida));
        }
    }
    
    public String getOpcionElegida(){
        return opcionElegida;
    }
    
    public void addOpcion(OpcionMenu opM){
        if(opcionesIntroducidas < opcionesDelMenu.length){
            opcionesDelMenu[opcionesIntroducidas] = opM;
            opcionesIntroducidas++;
        }
    }
    
    public void ejecutarOpcion(String id){
        for (OpcionMenu o : opcionesDelMenu) {
            if(o.getID().equals(id)){
                o.iniciar();
            }
        }
    }
}
