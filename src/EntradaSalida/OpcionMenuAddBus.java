package EntradaSalida;

import Excepciones.EntradaDatosException;
import Principal.BusInterurbano;
import Principal.BusUrbano;
import java.io.IOException;

public class OpcionMenuAddBus extends OpcionMenu{
        
    public OpcionMenuAddBus(String titulo){
        super(titulo, "ADDBUS");
    }
    
    @Override
    public void iniciar() {
        pintarCabezera();
        pedirEntradaDatos();
    }
    
    @Override
    public void pedirEntradaDatos() {
        int tipoBus = 0;
        int kilometros = 0;
        String ruta = "A";
        boolean seguirPidiendoTipoBus = true, seguirPidiendoKilometrosOrRuta = true, repetirMenu = true;
        
        do{
            try{
                if(seguirPidiendoTipoBus){
                    System.out.print("· Introduce el tipo de autobus que quieres dar de alta [1-Interurbano,2-Urbano] -> ");
                    tipoBus = Integer.parseInt(IN.readLine());
                    validarTipoBus(tipoBus);
                    seguirPidiendoTipoBus = false;
                }
                
                if(seguirPidiendoKilometrosOrRuta && tipoBus == 1){
                    System.out.print("· Introduce los kilometros que realiza el autobus [1-100] -> ");
                    kilometros = Integer.parseInt(IN.readLine());
                    validarKilometros(kilometros);
                    seguirPidiendoKilometrosOrRuta = false;
                    BASEDATOS.addBus(new BusInterurbano(kilometros));                    
                }else if(seguirPidiendoKilometrosOrRuta){
                    System.out.print("· Introduce la ruta que recorre el autobus [A-Z](ñ no incluida) -> ");
                    ruta = IN.readLine();
                    ruta = ruta.toUpperCase().trim();
                    validarRuta(ruta);
                    seguirPidiendoKilometrosOrRuta = false;
                    BASEDATOS.addBus(new BusUrbano(ruta.charAt(0)));                    
                }
                
                repetirMenu = pedirRepetirMenu("· ¿Quiere introducir otro autobús? [S/N] ->");
                if(repetirMenu){
                    seguirPidiendoTipoBus = true; seguirPidiendoKilometrosOrRuta = true; 
                }
            }catch(NumberFormatException e){
                System.err.println("!!!El tipo de dato introducido no es correcto!!!");
            }catch(EntradaDatosException | IOException e){
                System.err.println(e);
            }
        }while(repetirMenu);
    }

    private void validarTipoBus(int tipoBus) throws EntradaDatosException {
        
        if(tipoBus > 2 || tipoBus < 1){
            throw new EntradaDatosException(String.valueOf(tipoBus));
        }
    }
    
    private void validarKilometros(int kilometros) throws EntradaDatosException {
        
        if(kilometros > 100 || kilometros < 1){
            throw new EntradaDatosException(String.valueOf(kilometros));
        }
    }
    
    private void validarRuta(String rutaElegida) throws EntradaDatosException {
        
        if(!rutaElegida.matches("[A-Z]")){
            throw new EntradaDatosException(rutaElegida);
        }
    }
}
