package EntradaSalida;

import Excepciones.*;
import java.io.IOException;

public class OpcionMenuRemoveBus extends OpcionMenu{
    private final OpcionMenuInfo infoBuses;

    public OpcionMenuRemoveBus(String titulo, OpcionMenuInfo info){
        super(titulo, "DROPBUS");
        infoBuses = info;
    }
        
    @Override
    public void iniciar() {
        this.pintarCabezera();
        infoBuses.mostrarBuses();
        this.pedirEntradaDatos();
    }

    @Override
    public void pedirEntradaDatos() {
        int IDBus;
        boolean repetirMenu = true;
        
        do{
            try{
                System.out.print("· Introduce el ID del autobús que quieres borrar [0 para salir]->");
                IDBus = Integer.parseInt(IN.readLine());
                if(IDBus == 0){break;} //Añado un break ya que, si no hay autobuses dados de alta, no es posible salir del menu y 
                                       //me parece la opción más rápida sin tener que modificar mucho código. 
                validarID(IDBus);
                BASEDATOS.removeBus(IDBus);
                
                repetirMenu = pedirRepetirMenu("· ¿Quiere borrar otro autobus? [S/N] ->");
                
            }catch(NumberFormatException e){
                System.err.println("!!!El tipo de dato introducido no es correcto!!!");
            }catch(IDException | IOException e){
                System.err.println(e);
            }
        }while(repetirMenu);
    }
    
    private void validarID(int ID) throws IDException {
        if(!BASEDATOS.estaDadoDeAltaBus(ID)){
            throw new IDException(String.valueOf(ID));
        }
    }
}
