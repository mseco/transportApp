package EntradaSalida;

import Excepciones.EntradaDatosException;
import Principal.*;
import java.io.IOException;
import java.util.Iterator;

public class OpcionMenuInfo extends OpcionMenu{

    public OpcionMenuInfo(String titulo){
        super(titulo, "INFO");
    }
    
    @Override
    public void iniciar(){
        this.pintarCabezera();
        this.pedirEntradaDatos();
    }
    
    @Override
    public void pedirEntradaDatos(){
        int opcionElegida = 0;
        boolean repetirMenu = true;
        
        do{
            try{
                System.out.print("· Elige que información quieres ver [1-Autobuses, 2-Conductores] ->");
                opcionElegida = Integer.parseInt(IN.readLine());
                validarOpcionElegida(opcionElegida);
                if(opcionElegida == 1){
                    mostrarBuses();
                }else{
                    mostrarConductores();
                }
                repetirMenu = pedirRepetirMenu("· ¿Quiere consultar más información? [S/N] ->");
                
            }catch(NumberFormatException e){
                System.err.println("!!!El tipo de dato introducido no es correcto!!!");
            }catch(EntradaDatosException | IOException e){
                System.err.println(e);
            }
        }while(repetirMenu);
    }
    
    private void validarOpcionElegida(int opcionElegida) throws EntradaDatosException {
        
        if(opcionElegida > 2 || opcionElegida < 1){
            throw new EntradaDatosException(String.valueOf(opcionElegida));
        }
    }
    
    public void mostrarConductores(){
        System.out.println("ID    DNI          SUELDO    NOMBRE ");
        System.out.println("--    ---------    ------    ----------- ");
        
        Iterator i = BASEDATOS.getConductores().iterator();
        Conductor actual;
        while(i.hasNext()){
            actual = (Conductor) i.next();
            System.out.println(actual);
            }
        System.out.println("");        
    }
    
    public void mostrarBuses(){        
        System.out.println("ID    TIPO           INFORUTA      PRECIO     CONDUCTOR");
        System.out.println("--    -----------    ----------    ------     ---------");

        Iterator i = BASEDATOS.getBuses().iterator();
        Bus actual;
        while(i.hasNext()){
            actual =(Bus) i.next();
            System.out.println(actual);
        }
        System.out.println("");
    }
}
