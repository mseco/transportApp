package EntradaSalida;

import Excepciones.EntradaDatosException;
import Principal.BaseDatosTransportApp;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

public abstract class OpcionMenu {
    private final String titulo;
    private final String ID;
    protected static BufferedReader IN;
    protected final static BaseDatosTransportApp BASEDATOS = new BaseDatosTransportApp();
    
    static{
        try {
            IN = new BufferedReader(new InputStreamReader(System.in, "ISO-8859-1"));
        } catch (UnsupportedEncodingException ex) {
            IN = new BufferedReader(new InputStreamReader(System.in));
        }
    }
    
    public OpcionMenu(String t, String ID){
        titulo = t;
        this.ID = ID;     
    }
    
    public abstract void iniciar();
    public abstract void pedirEntradaDatos();

    public String getTitulo() {
        return titulo;
    }
    
    public String getID() {
        return ID;
    }
    
    protected void pintarCabezera(){
        //Se pinta la primera linea
        System.out.println("");
        for (int i = 0; i < 52; i++) {
            System.out.print("*");
        }
        
        //Se pinta la segunda linea
        System.out.print("\n***");
        for (int i = 0; i < ( (46-titulo.length()) / 2); i++) {
            System.out.print(" ");
        }
        System.out.print(titulo);
        for (int i = 0; i < ( (46-titulo.length()) / 2); i++) {
            System.out.print(" ");
        }
        if(titulo.length() % 2 != 0){System.out.print(" ***\n");}
        else{System.out.print("***\n");}
        
        //Se pinta la tercera linea
        for (int i = 0; i < 52; i++) {
            System.out.print("*");
        }
        System.out.println("");
    }
    
    protected boolean pedirRepetirMenu(String mensaje){
        String respuestaRepeticion;
        boolean seguirPidiendoEntrada = true;
        boolean repetirMenu = false;
        
        do{
            try{
                System.out.print(mensaje);
                respuestaRepeticion = IN.readLine();
                respuestaRepeticion = respuestaRepeticion.toLowerCase().trim();
                validarRespuestaRepeticion(respuestaRepeticion);
                repetirMenu = respuestaRepeticion.equals("s");
                seguirPidiendoEntrada = false;
            }catch(EntradaDatosException | IOException e){
                System.err.println(e);
            }
        }while(seguirPidiendoEntrada);
        
        return repetirMenu;
    }
    
    private void validarRespuestaRepeticion(String respuesta) throws EntradaDatosException{
        if(respuesta.length() > 2 || respuesta.length() ==  0){
            throw new EntradaDatosException(respuesta);
        }
        
        if(!(respuesta.equals("s") || respuesta.equals("n"))){
            throw new EntradaDatosException(respuesta);
        }
    }
}
