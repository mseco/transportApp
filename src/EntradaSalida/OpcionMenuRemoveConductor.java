package EntradaSalida;

import Excepciones.*;
import java.io.IOException;

public class OpcionMenuRemoveConductor extends OpcionMenu{
    private final OpcionMenuInfo infoConductores;

    public OpcionMenuRemoveConductor(String titulo, OpcionMenuInfo info){
        super(titulo, "DROPCONDUCTOR");
        infoConductores = info;
    }
        
    @Override
    public void iniciar() {
        this.pintarCabezera();
        infoConductores.mostrarConductores();
        this.pedirEntradaDatos();
    }

    @Override
    public void pedirEntradaDatos() {
        int IDConductor;
        boolean repetirMenu = true;
        
        do{
            try{
                System.out.print("· Introduce el ID del conductor que quieres borrar [0 para salir]->");
                IDConductor = Integer.parseInt(IN.readLine());
                if(IDConductor == 0){break;} //Añado un break ya que, si no hay conductores dados de alta, no es posible salir del menu y 
                                             //me parece la opción más rápida sin tener que modificar mucho código.               
                validarID(IDConductor);
                BASEDATOS.removeConductor(IDConductor);
                
                repetirMenu = pedirRepetirMenu("· ¿Quiere borrar otro conductor? [S/N] ->");
                
            }catch(NumberFormatException e){
                System.err.println("!!!El tipo de dato introducido no es correcto!!!");
            }catch(IDException | IOException e){
                System.err.println(e);
            }
        }while(repetirMenu);
    }
    
    private void validarID(int ID) throws IDException {
        if(!BASEDATOS.estaDadoDeAltaConductor(ID)){
            throw new IDException(String.valueOf(ID));
        }
    }
}

