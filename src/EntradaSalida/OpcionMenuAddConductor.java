package EntradaSalida;

import Excepciones.*;
import Principal.Conductor;
import java.io.IOException;

public class OpcionMenuAddConductor extends OpcionMenu{
    
    public OpcionMenuAddConductor(String titulo){
        super(titulo, "ADDCONDUCTOR");
    }
    
    @Override
    public void iniciar() {
        this.pintarCabezera();
        this.pedirEntradaDatos();
    }
    
    @Override
    public void pedirEntradaDatos() {
        int salario = 0;
        String nombre = "";
        String apellido1 = "";
        String apellido2 = "";
        String DNI = "";
        boolean seguirPidiendoNombre = true, seguirPidiendoApellido1 = true, seguirPidiendoApellido2 = true, seguirPidiendoSalario = true, seguirPidiendoDNI = true, repetirMenu = true;
        
        do{
            try{
                if(seguirPidiendoNombre){
                    System.out.print("· Introduce el nombre del conductor [máximo 30 caracteres] -> ");
                    nombre = IN.readLine();
                    nombre = nombre.toLowerCase().trim();
                    validarNombre(nombre);
                    nombre = nombre.substring(0, 1).toUpperCase() + nombre.substring(1, nombre.length());
                    seguirPidiendoNombre = false;
                }
                
                if(seguirPidiendoApellido1){
                    System.out.print("· Introduce el primer apellido de " + nombre + " [máximo 20 caracteres] -> ");
                    apellido1 = IN.readLine();
                    apellido1 = apellido1.toLowerCase().trim();
                    validarApellido(apellido1);
                    apellido1 = apellido1.substring(0, 1).toUpperCase() + apellido1.substring(1, apellido1.length());
                    seguirPidiendoApellido1 = false;
                } 
                
                if(seguirPidiendoApellido2){
                    System.out.print("· Introduce el segundo apellido de " + nombre + " [máximo 20 caracteres] -> ");
                    apellido2 = IN.readLine();
                    apellido2 = apellido2.toLowerCase().trim();
                    validarApellido(apellido2);
                    apellido2 = apellido2.substring(0, 1).toUpperCase() + apellido2.substring(1, apellido2.length());
                    seguirPidiendoApellido2 = false;
                } 
                
                if(seguirPidiendoSalario){
                    System.out.print("· Introduce el salario de " + nombre + " [725-2000]-> ");
                    salario = Integer.parseInt(IN.readLine());
                    validarSalario(salario);
                    seguirPidiendoSalario = false;
                }
                
                if(seguirPidiendoDNI){
                    System.out.print("· Introduce el DNI de " + nombre + ". -> ");
                    DNI = IN.readLine();
                    DNI = DNI.toUpperCase().trim();
                    validarDNI(DNI);
                    seguirPidiendoDNI = false;
                }
               
                BASEDATOS.addConductor(nombre, apellido1, apellido2, DNI, salario);
                
                repetirMenu = pedirRepetirMenu("· ¿Quiere introducir otro conductor? [S/N] ->");
                if(repetirMenu){
                    seguirPidiendoNombre = true; seguirPidiendoApellido1 = true; seguirPidiendoApellido2 = true; seguirPidiendoSalario = true; seguirPidiendoDNI = true;
                }
            }catch(NumberFormatException e){
                System.err.println("!!!El tipo de dato introducido no es correcto!!!");
            }catch(EntradaDatosException | LongitudCadenaInadecuadaException | IOException e){
                System.err.println(e);
            } catch (DniException e) {
                System.err.println(e);
                seguirPidiendoDNI = true;
            }
        }while(repetirMenu);
    }

    
    private void validarNombre(String nombre) throws EntradaDatosException, LongitudCadenaInadecuadaException {
        if(nombre.length() > 30 || nombre.length() == 0){
            throw new LongitudCadenaInadecuadaException(nombre.length());
        }
        
        int espaciosSeguidos = 0;
        for (int i = 0; i < nombre.length(); i++) {
            //se comprueba que el nombre no tenga dos espacios seguidos
            if(nombre.charAt(i) == ' '){
                espaciosSeguidos++;
            }else{
                espaciosSeguidos = 0;
            }
            if(espaciosSeguidos > 1){
                throw new EntradaDatosException("  ");
            }
            
            if( (nombre.charAt(i) < 97 || nombre.charAt(i) > 122)
                    && nombre.charAt(i) != 'ñ' && nombre.charAt(i) != 'á' && nombre.charAt(i) != 'ú'
                    && nombre.charAt(i) != 'é' && nombre.charAt(i) != 'í' && nombre.charAt(i) != 'ó'
                    && nombre.charAt(i) != ' '){
                throw new EntradaDatosException(String.valueOf(nombre.charAt(i)));
            }
        }
    }
    
    private void validarApellido(String apellido) throws EntradaDatosException, LongitudCadenaInadecuadaException {
        
        if(apellido.length() > 30 || apellido.length() == 0){
            throw new LongitudCadenaInadecuadaException(apellido.length());
        }
        
        for (int i = 0; i < apellido.length(); i++) {
            if( ( (apellido.charAt(i) < 97 || apellido.charAt(i) > 122)
                    && apellido.charAt(i) != 'ñ' && apellido.charAt(i) != 'á' && apellido.charAt(i) != 'ú'
                    && apellido.charAt(i) != 'é' && apellido.charAt(i) != 'í' && apellido.charAt(i) != 'ó')){
                throw new EntradaDatosException(String.valueOf(apellido.charAt(i)));
            }
        }
    }
    
    private void validarSalario(int salario) throws EntradaDatosException {
        
        if(salario > 2000 || salario < 725){
            throw new EntradaDatosException(String.valueOf(salario));
        }
    }
    
    private void validarDNI(String dni) throws DniException{
        String letras = "TRWAGMYFPDXBNJZSQVHLCKET";
        DniException e = new DniException(dni);
        
        if(!dni.matches("^[0-9]{8}[A-Z]$")){
            throw e;
        }
        
        char letraDelDNI = dni.charAt(dni.length()-1);
        int num = Integer.parseInt(dni.substring(0,(dni.length()-1)));
        
        //el resto de dividir el número por 23, nos dará el índice a la letra correspondiente
        int indice = (int) (num % 23); 
        if(letraDelDNI != letras.charAt(indice)){
            throw e;
        }
    }
}
