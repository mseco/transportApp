package EntradaSalida;

import Excepciones.*;
import Principal.Bus;
import java.io.IOException;

public class OpcionMenuImprimirTicket extends OpcionMenu {
    private final OpcionMenuInfo infoBuses;

    public OpcionMenuImprimirTicket(String titulo, OpcionMenuInfo info){
        super(titulo, "TICKET");
        infoBuses = info;        
    }
    
    @Override
    public void iniciar() {
        this.pintarCabezera();
        infoBuses.mostrarBuses();        
        this.pedirEntradaDatos();
    }

    @Override
    public void pedirEntradaDatos() {
        int idBusElegido = 0;
        boolean repetirMenu = true;
        
        do{
            try{
                System.out.print("· Introduce el identificador del autobús con el que quieres viajar [0 para salir]->");
                idBusElegido = Integer.parseInt(IN.readLine());
                if(idBusElegido == 0){break;} //Añado un break ya que, si no hay autobuses dados de alta, no es posible salir del menu y 
                                              //me parece la opción más rápida sin tener que modificar mucho código.                 
                validarID(idBusElegido);
                mostrarTicket(idBusElegido);
                repetirMenu = pedirRepetirMenu("· ¿Quiere pedir otro ticket? [S/N] ->");
                
            }catch(NumberFormatException e){
                System.err.println("!!!El tipo de dato introducido no es correcto!!!");
            }catch(IDException | IOException e){
                System.err.println(e);
            }
        }while(repetirMenu);
    }
    
    private void validarID(int ID) throws IDException {
        if(!BASEDATOS.estaDadoDeAltaBus(ID)){
            throw new IDException(String.valueOf(ID));
        }
    }
    
    private void mostrarTicket(int IDbus){
        Bus bus = BASEDATOS.getBus(IDbus);
        
        if(bus.getConductorAsignado() != null){        
            System.out.println("*Gracias por usar TransporAPP.\n*La información de su ticket es la siguiente:");
            System.out.println("  **Ha elegido viajar con el autobus: ID" + bus.getNUMERO_IDENTIFICADOR());
            System.out.println("  **El precio de su ticket es: " + bus.getPrecioBaseViaje() + "€");
            System.out.println("  **El conductor encargado del viaje es: " + bus.getConductorAsignado().getNombreCompleto() + "\n");
        }else{
            System.err.println("· No hay un conductor disponible para este autobus, disculpe las moslestias.");
        }
    }
}
