package Excepciones;

public class LongitudCadenaInadecuadaException extends Exception{
    private final int longitudCadena;
    
    public LongitudCadenaInadecuadaException(int lng){
        longitudCadena = lng;
    }
        
    @Override
    public String toString(){
        return "!!!La entrada de datos tiene una longitud inadecuada (" + longitudCadena + ")!!!";
    }
}
