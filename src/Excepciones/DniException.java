package Excepciones;

public class DniException extends Exception{
    private String dni;
    
    public DniException(String dni)
    {
        this.dni = dni;
    }
    
    public String getDNI(){
        return dni;
    }
    
    @Override
    public String toString()
    {
        return "El dni " + dni + " es incorrecto.";
    }
}

