package Excepciones;

public class IDException extends Exception{
    private String ID;
    
    public IDException(String id)
    {
        this.ID = id;
    }
    
    @Override
    public String toString()
    {
        return "El ID especificado es incorrecto (" + ID + ").";
    }
}

