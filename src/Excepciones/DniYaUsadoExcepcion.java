package Excepciones;

public class DniYaUsadoExcepcion extends DniException{
    
    public DniYaUsadoExcepcion(String dni)
    {
        super(dni);
    }
    
    @Override
    public String toString()
    {
        return "El dni " + getDNI() + " ya se encuentra registrado en nuestra base de datos.";
    }
}

