package Excepciones;

public class EntradaDatosException extends Exception{
    private final String datosIntroducidos;
    
    public EntradaDatosException(String d){
        datosIntroducidos = d;
    }
    
    @Override
    public String toString(){
        if(datosIntroducidos.equals(" ")){
            return "!!!La entrada de datos es incorrecta (\" \")!!!";
        }else{
            return "!!!La entrada de datos es incorrecta (" + datosIntroducidos + ")!!!";
        }
    }
}
