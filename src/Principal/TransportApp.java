package Principal;

import EntradaSalida.*;

public class TransportApp {
    private final Menu menuPrincipal;
    
    public TransportApp(){
        menuPrincipal = new Menu("BIENVENIDO AL MENÚ PRINCIPAL DE TRANSPORTAPP", 6);
        
        menuPrincipal.addOpcion(new OpcionMenuAddBus("DAR DE ALTA AUTOBUSES"));
        menuPrincipal.addOpcion(new OpcionMenuAddConductor("DAR DE ALTA CONDUCTORES"));
        OpcionMenuInfo menuInfo = new OpcionMenuInfo("OBTENER INFORMACIÓN");
        menuPrincipal.addOpcion(menuInfo);
        menuPrincipal.addOpcion(new OpcionMenuRemoveBus("DAR DE BAJA AUTOBUSES", menuInfo));
        menuPrincipal.addOpcion(new OpcionMenuRemoveConductor("DAR DE BAJA CONDUCTORES", menuInfo));
        menuPrincipal.addOpcion(new OpcionMenuImprimirTicket("OBTENER TICKETS", menuInfo));
    }
    
    public void ejecutar(){
        String opcionElegida;
        
        do{
            menuPrincipal.iniciar();
            opcionElegida = menuPrincipal.getOpcionElegida();
            
            switch(opcionElegida){
                case "ADDBUS":
                    menuPrincipal.ejecutarOpcion("ADDBUS");
                    break;
                case "ADDCONDUCTOR":
                    menuPrincipal.ejecutarOpcion("ADDCONDUCTOR");
                    break;
                case "DROPBUS":
                    menuPrincipal.ejecutarOpcion("DROPBUS");
                    break;
                case "DROPCONDUCTOR":
                    menuPrincipal.ejecutarOpcion("DROPCONDUCTOR");
                    break;
                case "INFO":
                    menuPrincipal.ejecutarOpcion("INFO");
                    break;
                case "TICKET":
                    menuPrincipal.ejecutarOpcion("TICKET");
                    break;
            }
        }while(!opcionElegida.equals("SALIR"));
    }
}
