package Principal;

public class Conductor implements Comparable{ 
    private static int conductoresCreados = 0;
    
    private final int NUMERO_IDENTIFICADOR;
    private final String nombre, apellido1, apellido2;
    private final String DNI;
    private int salario;
    private boolean tieneBusAsignado;

    public Conductor(String nom, String ape1, String ape2, String DNI, int salar){
        conductoresCreados++;
        
        NUMERO_IDENTIFICADOR = conductoresCreados;
        nombre = nom;
        apellido1 = ape1;
        apellido2 = ape2;
        this.DNI = DNI;
        salario = salar;
        tieneBusAsignado = false;
    }
    
    public String getNombre() {
        return nombre;
    }

    public String getApellido1() {
        return apellido1;
    }

    public String getApellido2() {
        return apellido2;
    }
    
    public String getNombreCompleto(){
        return nombre + " " + apellido1 + " " + apellido2;
    }

    public int getNUMERO_IDENTIFICADOR() {
        return NUMERO_IDENTIFICADOR;
    }
    
    public boolean getTieneBusAsignado() {
        return tieneBusAsignado;
    }
    
    public String getDNI(){
        return DNI;
    }
    
    public void setTieneBusAsignado(Boolean t) {
        tieneBusAsignado = t;
    }
    
    public void setSalario(int s){
        salario = s;
    }
    
    @Override
    public String toString(){
        String s = String.format("%2d    %9s    %5d€    %s", NUMERO_IDENTIFICADOR, DNI, salario, getNombreCompleto());
        return s;
    }

    @Override
    public int compareTo(Object o) {
        Conductor con = (Conductor)o;
        
        if(this.NUMERO_IDENTIFICADOR > con.NUMERO_IDENTIFICADOR){
            return 1;
        }else if(this.NUMERO_IDENTIFICADOR == con.NUMERO_IDENTIFICADOR){
            return 0;
        }else{
            return -1;
        }
    }
}
