package Principal;

public class BusUrbano extends Bus{
    private final char ruta;
    private final double precioBaseViaje;
    
    public BusUrbano(char ruta){
        this.ruta = ruta;
        precioBaseViaje = 2.0;
    }
    
    @Override
    public double getPrecioBaseViaje() {
        if(ruta == 'A'){
            return (precioBaseViaje * 1.1);
        }else{
            return (precioBaseViaje * 1.2);
        }
    }
    
    @Override
    public String toString(){
        String infoConductor = super.toString();
        
        String s = String.format("%2d    Urbano         Ruta %5s    %5.2f€ %s", getNUMERO_IDENTIFICADOR(), ruta, getPrecioBaseViaje(), infoConductor);
        return s;
    }
}