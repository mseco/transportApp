package Principal;

import Excepciones.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class BaseDatosTransportApp {
    private ArrayList buses;
    private ArrayList conductoresEnActivo;
    private ArrayList historialConductores; //incluye todos los conductores que se dieron de alta alguna vez
    
    public BaseDatosTransportApp(){
        buses = new ArrayList();
        conductoresEnActivo = new ArrayList();
        historialConductores = new ArrayList();
    }
    
    public void addBus(Bus b){
        buses.add(b);
        emparejar();
    }
    
    public void addConductor(String nom, String ape1, String ape2, String DNI, int salario) throws DniYaUsadoExcepcion{
        Conductor conComprobado;
        
        //primero se verifica si el dni lo esta usando otro conductor en activo
        Iterator i = conductoresEnActivo.iterator();
        while(i.hasNext()){
            conComprobado =(Conductor) i.next();
            if(conComprobado.getDNI().equals(DNI)){
                throw new DniYaUsadoExcepcion(DNI);
            }
        }
        
        //luego se comprueba si existe un conductor en el historial con el mismo dni
        //si es así, se verifica si el conductore estuvo dado de alta alguna vez
        //si es así, se rescata el objeto, se actualiza con el nuevo salario, y se añade a conductoresEnActivo.
        boolean estaEnHistorial = false;        
        Iterator y = historialConductores.iterator();
        while(y.hasNext()){
            conComprobado =(Conductor) y.next();
            if(conComprobado.getDNI().equals(DNI)){ 
                if(!conComprobado.getNombreCompleto().equals(nom + " " + ape1 + " " + ape2)){
                    throw new DniYaUsadoExcepcion(DNI);
                }else{
                    conComprobado.setSalario(salario);
                    conductoresEnActivo.add(conComprobado);
                    estaEnHistorial = true;
                    Collections.sort(conductoresEnActivo);
                }    
            }
        }        
        
        if(!estaEnHistorial){
            Conductor c = new Conductor(nom, ape1, ape2, DNI, salario);
            conductoresEnActivo.add(c);
            historialConductores.add(c);
        }
        emparejar();
    }
    
    public void removeBus(int id){
        Bus busActual;
        Iterator i = buses.iterator();
        
        while(i.hasNext()){
            busActual = (Bus)i.next();
            if(busActual.getNUMERO_IDENTIFICADOR() == id){
                //antes de borrar el autobus comprobamos si tiene un conductor asociado y si es así actualizamos datos
                if(busActual.tieneConductorAsignado() == true){
                    busActual.getConductorAsignado().setTieneBusAsignado(false); 
                }
                i.remove();
            }
        }
        emparejar();
    }
    
    public void removeConductor(int id){
        Conductor conActual;
        Iterator i = conductoresEnActivo.iterator();
        
        while(i.hasNext()){
            conActual = (Conductor)i.next();
            if(conActual.getNUMERO_IDENTIFICADOR() == id){
                if(conActual.getTieneBusAsignado() == true){
                getBus(conActual).setConductorAsignado(null); //se actualiza la información del autobus al que estaba asignado
                conActual.setTieneBusAsignado(false); //se actualiza la información del conductor por si en un futuro es dado de alta de nuevo
                }
                i.remove();
            }
        }
        emparejar();
    }
    
    private void emparejar(){
        Conductor conductorActual;
        Bus busActual;
        
        Iterator c = conductoresEnActivo.iterator();
        Iterator b = buses.iterator();
        
        while(c.hasNext() && b.hasNext()){
            conductorActual =(Conductor)c.next();
            while((!conductorActual.getTieneBusAsignado()) && b.hasNext()){
                busActual =(Bus) b.next();
                if(busActual.getConductorAsignado() == null) {
                    busActual.setConductorAsignado(conductorActual);
                    conductorActual.setTieneBusAsignado(true);
                } 
            }
        }
    }
    
    public ArrayList getConductores(){
        return conductoresEnActivo;
    }
    
    public ArrayList getBuses(){
        return buses;
    }
    
    public Bus getBus(int id){
        Iterator i = buses.iterator();
        Bus busActual = null;
        boolean seguir = true;
        while(i.hasNext() && seguir){
            busActual =(Bus) i.next();
            if(busActual.getNUMERO_IDENTIFICADOR() == id) {
                seguir = false;
            } 
        }
        return busActual;
    }

    public Bus getBus(Conductor c){
        Iterator i = buses.iterator();
        Bus busActual = null;
        boolean seguir = true;
        while(i.hasNext() && seguir){
            busActual =(Bus) i.next();
            if(busActual.getConductorAsignado() != null && busActual.getConductorAsignado().equals(c)) {
                seguir = false;
            } 
        }
        return busActual;
    }    
    
    public boolean estaDadoDeAltaConductor(int ID){
        boolean estaDado = false;
        
        Iterator i = getConductores().iterator();
        Conductor actual;
        while(i.hasNext()){
            actual =(Conductor) i.next();
            if(actual.getNUMERO_IDENTIFICADOR() == ID){
                estaDado = true;
            }
        }
        return estaDado;
    }
    
    public boolean estaDadoDeAltaBus(int ID){
        boolean estaDado = false;
        
        Iterator i = getBuses().iterator();
        Bus actual;
        while(i.hasNext()){
            actual =(Bus) i.next();
            if(actual.getNUMERO_IDENTIFICADOR() == ID){
                estaDado = true;
            }
        }
        return estaDado;
    }
}
