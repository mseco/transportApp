package Principal;

public abstract class Bus {
    private static int busesCreados = 0;
    
    private final int NUMERO_IDENTIFICADOR;
    private Conductor conductorAsignado;
    
    public Bus(){
        busesCreados++;
        NUMERO_IDENTIFICADOR= busesCreados;
        conductorAsignado = null;
    }

    public abstract double getPrecioBaseViaje();
    
    public int getNUMERO_IDENTIFICADOR() {
        return NUMERO_IDENTIFICADOR;
    }

    public Conductor getConductorAsignado() {
        return conductorAsignado;
    }
    
    public boolean tieneConductorAsignado(){
        if(conductorAsignado == null){return false;}
        else {return true;}
    }

    public void setConductorAsignado(Conductor conductor) {
        this.conductorAsignado = conductor;
    }
    
    @Override
    public String toString(){
        String infoConductor;
        if(conductorAsignado == null){
            infoConductor = "    Sin conductor";
        }else{
            infoConductor = "    " + conductorAsignado.getNombreCompleto();
        }
        return infoConductor;
    }
}
