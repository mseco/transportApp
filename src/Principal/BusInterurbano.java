package Principal;

public class BusInterurbano extends Bus{
    private final int kilometrosRuta;
    private final double precioBaseViaje;

    
    public BusInterurbano(int kilometros){
        kilometrosRuta = kilometros;
        precioBaseViaje = 0.2;
    }
    
    @Override
    public double getPrecioBaseViaje() {
        return (kilometrosRuta * precioBaseViaje);
    }
    
    @Override
    public String toString(){
        String infoConductor = super.toString();
        
        String s = String.format("%2d    Interurbano    Ruta %3dkm    %5.2f€ %s", getNUMERO_IDENTIFICADOR(), kilometrosRuta, getPrecioBaseViaje(), infoConductor);
        return s;
    }
}
